import json
import urllib
from time import sleep

from Direction import Direction

class Waze(object):
    # %3A before coordinate
    URL_FORMAT = "https://www.waze.com/il-RoutingManager/routingRequest?from=x%3A{from_lat}+y%3A{from_lon}&to=x%3A{to_lat}+y%3A{to_lon}&at=0&returnJSON=true&returnGeometries=true&returnInstructions=true&timeout=60000&nPaths={num_paths}&options=AVOID_TRAILS%3At"

    def get_json_response_from_waze(self, from_point, to_point, num_of_paths=1):
        url = Waze.URL_FORMAT.format(from_lat=from_point.x,
                                     from_lon=from_point.y,
                                     to_lat=to_point.x,
                                     to_lon=to_point.y,
                                     num_paths=num_of_paths)
        for i in xrange(3):
            raw_result = urllib.urlopen(url).read()
            try:
                return json.loads(raw_result)
            except ValueError:
                sleep(1)
        raise EnvironmentError("Could not fetch ")


    def get_route(self, from_point, to_point):
        json_response = self.get_json_response_from_waze(from_point, to_point)
        street_ids_to_names = json_response[u"response"][u"streetNames"]
        directions = []
        previous_street = None
        for direction in json_response[u"response"][u"results"]:
            current_street = direction[u"street"]
            current_street_name = street_ids_to_names[current_street]
            if not current_street_name:
                continue
            new_direction = Direction(street=current_street_name,
                                      cross_time_secs=direction[u"crossTime"],
                                      cross_time_no_realtime_secs=direction[u"crossTimeWithoutRealTime"],
                                      distance_meters=direction[u"distance"],
                                      length_meters=direction[u"length"])
            if previous_street != current_street:
                directions.append(new_direction)
            else:
                directions[-1] += new_direction
            previous_street = direction[u"street"]
        return directions

    def get_routes_directions(self, src_dest_list):
        return map(lambda src_dest_point_tuple:
                   self.get_route(src_dest_point_tuple[0], src_dest_point_tuple[1]),
                   src_dest_list)
