import json

from flask import Flask
from Point import Point
from Route import Route
from Waze import Waze

app = Flask(__name__)

@app.route('/get/<name>/<float:src_x>_<float:src_y>/<float:dst_x>_<float:dst_y>')
def get_route(name, src_x, src_y, dst_x, dst_y):
    waze = Waze()
    src_dst_points = [(Point(src_x, src_y), Point(dst_x, dst_y))]
    routes_directions = waze.get_routes_directions(src_dst_points)
    return json.dumps(Route(name, routes_directions[0]).get_sum_dict())

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000')

