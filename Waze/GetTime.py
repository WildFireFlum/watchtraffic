from __future__ import print_function
import json
from os import path
from Point import Point
from Route import Route
from Waze import Waze

CONFIG_FILE_NAME = "config.json"


def get_single_use_output(route_list):
    return map(lambda route: route.get_sum_dict(), route_list)


def print_all_routes(route_list):
    return map(lambda route: route.pretty_print(), route_list)

if __name__ == "__main__":
    waze = Waze()
    config = None
    with open(path.join(path.dirname(__file__), CONFIG_FILE_NAME), 'rb') as config_file:
        config = json.loads(config_file.read())
    src_dst_points = [(Point(route["src_x"], route["src_y"]),
                       Point(route["dst_x"], route["dst_y"]))
                      for route in config["routes"]]
    route_names = [route["name"] for route in config["routes"]]
    routes_directions = waze.get_routes_directions(src_dst_points)
    routes = [Route(name, directions) for name, directions in zip(route_names, routes_directions)]
    json_routes = get_single_use_output(routes)
    print_all_routes(routes)
