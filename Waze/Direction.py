class Direction(object):
    def __init__(self, street, cross_time_secs, cross_time_no_realtime_secs, distance_meters, length_meters):
        self._street = street.encode('utf-8')
        self._cross_time_mins = cross_time_secs / 60.0
        self._cross_time_no_realtime_mins = cross_time_no_realtime_secs / 60.0
        self._distance_km = distance_meters / 1000.0
        self._length_km = length_meters / 1000.0

    @property
    def street(self):
        return self._street

    @property
    def cross_time_mins(self):
        return self._cross_time_mins

    @property
    def cross_time_no_realtime_mins(self):
        return self._cross_time_no_realtime_mins

    @property
    def distance_km(self):
        return self._distance_km

    @property
    def length_km(self):
        return self._length_km

    def to_json_dict(self):
        return {
                    "street": self._street,
                    "cross_time_mins": self._cross_time_mins,
                    "cross_time_no_realtime_mins": self._cross_time_no_realtime_mins,
                    "distance_km": self._distance_km,
                    "length_km": self._length_km,
               }

    def short_pretty_string(self):
        return 'street: {street} - time: {cross_time:.2f} Mins - distance: {distance:.2f} Kms'\
            .format(street=self._street,
                    cross_time=self._cross_time_mins,
                    distance=self._cross_time_mins)

    def __iadd__(self, other):
        assert self._street == other.street, \
            "Direction streets don't match\n self: {0}\n other: {1}".format(repr(self._street), repr(other.street))
        self._cross_time_mins += other.cross_time_mins
        self._cross_time_no_realtime_mins += other.cross_time_no_realtime_mins
        self._distance_km += other.distance_km
        self._length_km += other.length_km
        return self
