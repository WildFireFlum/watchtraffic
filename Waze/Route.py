from __future__ import print_function


class Route:
    def __init__(self, name, direction_list):
        self._name = name
        self._directions = direction_list

    def get_sum_dict(self):
        return {
                "name": self._name,
                "length": sum([d.length_km for d in self._directions]),
                "time": sum([d.cross_time_mins for d in self._directions]),
                "time_no_realtime": sum([d.cross_time_no_realtime_mins for d in self._directions]),
                "src": self._directions[0].street,
                "dest": self._directions[-1].street
                #"directions": map(lambda direction: direction.to_json_dict(), self._directions)
               }

    def pretty_print(self):
        print("Route {name}".format(name=self._name))
        print("=====================")
        print("Total length: {0:.2f} km"
              .format(sum([d.length_km for d in self._directions])))
        print("Total time: {0:.2f} mins"
              .format(sum([d.cross_time_mins for d in self._directions])))
        print("Total time (w/o real time): {0:.2f} mins"
              .format(sum([d.cross_time_no_realtime_mins for d in self._directions])))
        map(lambda direction: print(direction.short_pretty_string()), self._directions)
        print("")

