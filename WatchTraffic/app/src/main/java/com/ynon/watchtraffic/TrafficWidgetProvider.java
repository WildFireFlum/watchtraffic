package com.ynon.watchtraffic;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import java.text.MessageFormat;

public class TrafficWidgetProvider extends AppWidgetProvider {

    public PendingIntent getRefreshPendingIntent(Context context, int appWidgetId){
        Intent intent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void updateMyWidget(AppWidgetManager appWidgetManager, Context context,
                               RemoteViews remoteViews, Integer widgetId) {
        try {
            Log.d("updateMyWidget", "Displaying load");

            remoteViews.setViewVisibility(R.id.all_traffic_view, View.INVISIBLE);
            remoteViews.setViewVisibility(R.id.progressBar, View.VISIBLE);
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.layout.traffic_widget);
            appWidgetManager.partiallyUpdateAppWidget(widgetId, remoteViews);

            Log.d("updateMyWidget", "Getting info");
            HTMLGetter htmlGetter = new HTMLGetter();
            Thread networkingThread = new Thread(htmlGetter);
            networkingThread.start();

            String src = "";
            String dest = "";
            Double time = 0.0;
            Double length = 0.0;
            try {
                networkingThread.join();
                Log.d("updateMyWidget",
                        MessageFormat.format("Fetched JSON: {0}", htmlGetter.getJsonFromHTML()));
                JsonObject jsonObject = Json.parse(htmlGetter.getJsonFromHTML()).asObject();
                src   = jsonObject.getString("src", "");
                dest   = jsonObject.getString("dest", "");
                length = jsonObject.getDouble("length", 0.0);
                time   = jsonObject.getDouble("time", 0.0);
            }
            catch (RuntimeException e) {
                Log.e("JSONParseFail", e.getMessage());
                e.printStackTrace();
            } catch (InterruptedException e) {
                Log.e("HTMLGetFail", e.getMessage());
                e.printStackTrace();
            }

            remoteViews.setTextViewText(R.id.src_street,
                    MessageFormat.format("{0} {1}", context.getString(R.string.src_street), src));
            remoteViews.setTextViewText(R.id.dst_street,
                    MessageFormat.format("{0} {1}", context.getString(R.string.dst_street), dest));
            remoteViews.setTextViewText(R.id.real_time,
                    MessageFormat.format("{0} {1} Mins", context.getString(R.string.real_time), time));
            remoteViews.setTextViewText(R.id.length,
                    MessageFormat.format("{0} {1} Kms", context.getString(R.string.length), length));

            Log.d("updateMyWidget",  MessageFormat.format("AFTER UPDATE {0} {1} Mins", context.getString(R.string.real_time), time));
            remoteViews.setViewVisibility(R.id.progressBar, View.INVISIBLE);
            remoteViews.setViewVisibility(R.id.all_traffic_view, View.VISIBLE);
            remoteViews.setOnClickPendingIntent(R.id.refreshButton, getRefreshPendingIntent(context, widgetId));
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.layout.traffic_widget);
            appWidgetManager.partiallyUpdateAppWidget(widgetId, remoteViews);
        }
        catch (Exception e) {
            Log.e("AppAPIFail", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals("android.appwidget.action.APPWIDGET_UPDATE")){
            Log.d("OnReceive", "Updating widget");
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int widgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(context, TrafficWidgetProvider.class));
            for (int id : widgetIds) {
                RemoteViews remoteViews =
                        new RemoteViews(context.getPackageName(), R.layout.traffic_widget);
                updateMyWidget(appWidgetManager, context, remoteViews, id);
            }
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final Integer nb = appWidgetIds.length;
        Log.d("BeginOnUpdate", "Updating widgets");
        for (int i = 0; i < nb; i++) {
            Log.d("OnUpdate", "Updating widget");
            RemoteViews remoteViews =
                    new RemoteViews(context.getPackageName(), R.layout.traffic_widget);
            updateMyWidget(appWidgetManager, context, remoteViews, appWidgetIds[i]);
        }
    }
}
