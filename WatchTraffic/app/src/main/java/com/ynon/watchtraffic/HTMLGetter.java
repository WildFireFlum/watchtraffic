package com.ynon.watchtraffic;

import android.util.Log;

import org.jsoup.Jsoup;
import java.io.IOException;

public class HTMLGetter implements Runnable
{
    private volatile String html = "wahey";

    @Override
    public void run() {
        try {
            html = Jsoup.connect(TrafficConfig.URL).get().body().ownText();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("HTMLFetchError", e.getMessage());
        }
    }

    public String getJsonFromHTML() {
        return html;
    }
}


